# One Small Pen Plotter

![penplotter](images/pplotter-2.jpg)

[CAD Files !](cad/pplotter)

It's a nice pen holder, it's a bit absurd. Currently: stray linear bearings found in CBA basement (very flash), could be LM6UU's or similar. Linear bearings + flexure add compliance in the Z axis, meaning we dont' break our pens.

- do lm6uu design for repeatability elsewhere 
- could make pen-clip friendlier / faster 
- could be one big flexure 